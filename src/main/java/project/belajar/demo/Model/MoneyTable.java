package project.belajar.demo.Model;

import project.belajar.demo.Core.Product.FintechCash;
import project.belajar.demo.Core.Product.FintechDana;
import project.belajar.demo.Core.Product.FintechGopay;
import project.belajar.demo.Core.Product.FintechOvo;

import javax.persistence.*;

@Entity
@Table(name = "Money")
public class MoneyTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "code", unique = true, nullable = false)
    private String code;

    @Column(name = "nameOfPeople", nullable = false)
    private String nameOfPeople;

    @Column(name = "nameOfFintech", nullable = false)
    private String nameOfFintech;

//    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @Column(name = 'fintech')
//    @PrimaryKeyJoinColumn
//    private <Set> fintechMoney = new <Set> (FintechCash, FintechDana, FintechOvo, FintechGopay)

    public MoneyTable(){}

    public MoneyTable(String code, String nameOfFintech, String nameOfPeople) {
        this.code = code;
        this.nameOfFintech = nameOfFintech;
        this.nameOfPeople = nameOfPeople;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNameOfFintech() {
        return nameOfFintech;
    }

    public void setNameOfFintech(String name) {
        this.nameOfFintech = name;
    }

    public String getNameOfPeople() {
        return nameOfPeople;
    }

    public void setNameOfPeople(String name) {
        this.nameOfPeople = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
