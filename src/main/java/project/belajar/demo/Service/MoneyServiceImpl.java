package project.belajar.demo.Service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.belajar.demo.Core.MoneyFactory;
import project.belajar.demo.Core.Product.Money;
import project.belajar.demo.Model.MoneyTable;
import project.belajar.demo.Repository.MoneyRepository;

import java.util.List;

@Service
public class MoneyServiceImpl implements MoneyService {

    private Money fintechMoney;
    private MoneyFactory moneyFactory;

    @Autowired
    private MoneyRepository moneyRepository;

    @Override
    public String findByNameOfFintech(String nameFintech) {
        return (nameFintech);
    }

    @Override
    public List<Money> findAll() {
        return null;
    }

    @Override
    public void setFintechMoney(String fintechName) {

        moneyFactory = new MoneyFactory();

        fintechMoney = moneyFactory.getMoney(fintechName);

        MoneyTable moneyTable = new MoneyTable();

        moneyTable.setNameOfPeople(fintechMoney.getNamePeople());
        moneyTable.setNameOfFintech(fintechMoney.getNameFintech());
        moneyTable.setCode(fintechMoney.getCode() + (int)(Math.random()));

        moneyRepository.save(moneyTable);
    }
}