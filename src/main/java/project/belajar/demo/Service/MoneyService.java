package project.belajar.demo.Service;

import project.belajar.demo.Core.Product.Money;
import project.belajar.demo.Model.MoneyTable;

import java.util.List;

public interface MoneyService {

    public String findByNameOfFintech(String nameFintech);

    public List<Money> findAll();

    public void setFintechMoney(String fintechName);

}
