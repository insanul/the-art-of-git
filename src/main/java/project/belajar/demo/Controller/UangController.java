package project.belajar.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import project.belajar.demo.Service.MoneyService;

@Controller
@RequestMapping(path = "/uang")
public class UangController {

    @Autowired
    private MoneyService moneyService;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    private String uangMethod1() {
        return "home";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/apalah")
    private String uangMethod2() {
        return "SplitBill";
    }
}
