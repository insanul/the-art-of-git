package project.belajar.demo.Core.Product;

public class FintechOvo implements Money{

    @Override
    public void createMoney() {

        System.out.println("Create Ovo");
    }

    @Override
    public String getCode() {
        return "003";
    }

    @Override
    public String getNameFintech() {
        return "OVO";
    }

    @Override
    public String getNamePeople() {
        return "A";
    }
}
