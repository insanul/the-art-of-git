package project.belajar.demo.Core.Product;

public class FintechDana implements Money {

    @Override
    public void createMoney() {

        System.out.println("Create Dana");
    }

    @Override
    public String getCode() {
        return "001";
    }

    @Override
    public String getNameFintech() {
        return "DANA";
    }

    @Override
    public String getNamePeople() {
        return "A";
    }
}
