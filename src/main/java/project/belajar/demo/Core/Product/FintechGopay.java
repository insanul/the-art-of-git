package project.belajar.demo.Core.Product;

public class FintechGopay implements Money {

    @Override
    public void createMoney() {

        System.out.println("Create Gopay");
    }

    @Override
    public String getCode() {
        return "002";
    }

    @Override
    public String getNameFintech() {
        return "GOPAY";
    }

    @Override
    public String getNamePeople() {
        return "A";
    }
}
