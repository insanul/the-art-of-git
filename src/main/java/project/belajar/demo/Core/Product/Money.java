package project.belajar.demo.Core.Product;

public interface Money {
    void createMoney();
    public String getCode();
    public String getNameFintech();
    public String getNamePeople();
}
