package project.belajar.demo.Core.Product;

import project.belajar.demo.Service.MoneyService;

public class FintechCash implements Money {

    @Override
    public void createMoney() {

        System.out.println("Create Cash");
    }

    @Override
    public String getCode() {
        return "000";
    }

    @Override
    public String getNameFintech() {
        return "CASH";
    }

    @Override
    public String getNamePeople() {
        return "A";
    }
}
