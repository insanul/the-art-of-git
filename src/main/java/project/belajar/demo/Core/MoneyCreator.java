package project.belajar.demo.Core;


import project.belajar.demo.Core.Product.Money;

public class MoneyCreator {
    public static void main(String[] args) {
        MoneyFactory moneyFactory = new MoneyFactory();

        //get an object of FintechCash and call its create method.
        Money money1 = moneyFactory.getMoney("CASH");

        //call create method of FintechCash
        money1.createMoney();

        //get an object of FintechOvo and call its create method.
        Money money2 = moneyFactory.getMoney("OVO");

        //call create method of FinechOvo
        money2.createMoney();

        //get an object of FintechDana and call its create method.
        Money money3 = moneyFactory.getMoney("DANA");

        //call create method of FintechDana
        money3.createMoney();

        //get an object of FintechGopay and call its create method.
        Money money4 = moneyFactory.getMoney("GOPAY");

        //call create method of FintechGopay
        money4.createMoney();
    }
}
