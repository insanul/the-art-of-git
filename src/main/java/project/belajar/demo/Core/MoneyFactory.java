package project.belajar.demo.Core;

import project.belajar.demo.Core.Product.*;

public class MoneyFactory {

    //use getMoney method to get object of type Money
    public Money getMoney(String moneyType){

        if(moneyType == null){
            return null;
        }
        if(moneyType.equalsIgnoreCase("CASH")){
            return new FintechCash();

        } else if(moneyType.equalsIgnoreCase("OVO")){
            return new FintechOvo();

        } else if(moneyType.equalsIgnoreCase("GOPAY")){
            return new FintechGopay();

        } else if(moneyType.equalsIgnoreCase("DANA")){
            return new FintechDana();
        }

        return null;
    }
}
