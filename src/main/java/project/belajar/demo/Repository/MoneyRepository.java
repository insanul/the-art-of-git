package project.belajar.demo.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import project.belajar.demo.Core.Product.Money;
import project.belajar.demo.Model.MoneyTable;

public interface MoneyRepository extends JpaRepository<MoneyTable, Long> {
}
